<div class="table-responsive">
    <table id="attendance-summary-details-table" class="display" cellspacing="0" width="100%">            
        <tfoot>
            <tr>
                <th colspan="3" class="text-right"><?php echo lang("total") ?>:</th>
                <th data-current-page="3"></th>
                <th data-current-page="4"></th>

            </tr>
            <tr data-section="all_pages">
                <th colspan="3" class="text-right"><?php echo lang("total_of_all_pages") ?>:</th>
                <th data-all-page="3"></th>
                <th data-all-page="4"></th>
            </tr>
        </tfoot>
    </table>
</div>
<script type="text/javascript">
    $(document).ready(function () {
        $("#attendance-summary-details-table").appTable({
            source: '<?php echo_uri("attendance/summary_details_list_data/"); ?>',
            order: [[0, "asc"]],
            filterDropdown: [{name: "user_id", class: "w200", options: <?php echo $team_members_dropdown; ?>}],
            rangeDatepicker: [{startDate: {name: "start_date", value: moment().format("YYYY-MM-DD")}, endDate: {name: "end_date", value: moment().format("YYYY-MM-DD")}}],
            columns: [
                {visible: false, searchable: false},
                {title: "<?php echo lang("team_member"); ?>", "iDataSort": 0},
                {title: "<?php echo lang("date"); ?>", "bSortable": false, "class": "w20p"},
                {title: "<?php echo lang("duration"); ?>", "bSortable": false, "class": "w20p"},
                {title: "<?php echo lang("hours"); ?>",  "bSortable": false,"class": "w20p"}
            ],
            printColumns: [ 1, 2, 3, 4],
            xlsColumns: [ 1, 2, 3, 4],
            summation: [{column: 3, dataType: 'time'}, {column: 4, dataType: 'number'}]
        });
    });
</script>