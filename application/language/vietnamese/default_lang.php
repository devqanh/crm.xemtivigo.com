<?php

///* LƯU Ý: KHÔNG THAY ĐỔI FILE NÀY. NẾU BẠN MUỐN cập nhật ngôn ngữ sau đó sao chép tập tin này để custom_lang.php VÀ CẬP NHẬT CÓ */
//
///* Ngôn ngữ miền địa phương */
$lang["language_locale"] = "en"; // mã miền địa phương
$lang["language_locale_long"] = "en-US"; // mã miền địa phương dài

/* chung */
$lang["add"] = "Add";
$lang["chỉnh sửa"] = "Edit";
$lang["gần"] = "Close";
$lang["hủy"] = "Hủy bỏ";
$lang["tiết kiệm"] = "Save";
$lang["xóa"] = "Delete";
$lang["mô tả"] = "Mô tả";
$lang["admin"] = "Quản trị";
$lang["người quản lý"] = "Quản lý";
$lang["tùy chọn"] = "Tùy chọn";
$lang["id"] = "ID";
$lang["name"] = "Tên";
$lang["email"] = "Email";
$lang["username"] = "Tên đăng nhập";
$lang["password"] = "Mật khẩu";
$lang["retype_password"] = "Nhập lại mật khẩu";
$lang["trước"] = "trước";
$lang["bên cạnh"] = "Next";
$lang["active"] = "Active";
$lang["không hoạt động"] = "Không hoạt động";
$lang["trạng thái"] = "Tình trạng";
$lang["start_date"] = "Ngày bắt đầu";
$lang["end_date"] = "Ngày kết thúc";
$lang["start_time"] = "thời gian Bắt đầu";
$lang["END_TIME"] = "End time";
$lang["hạn chót"] = "Thời hạn";
$lang["thêm"] = "gia tăng";
$lang["CREATED_DATE"] = "Ngày tạo";
$lang["tạo ra"] = "Đã tạo";
$lang["created_by"] = "Created by";
$lang["cập nhật"] = "Cập nhật";
$lang["xóa"] = "xóa";
$lang["tệ"] = "tệ";
$lang["mới"] = "New";
$lang["mở"] = "Open";
$lang["khép kín"] = "Closed";
$lang["ngày"] = "Ngày";
$lang["yes"] = "Yes";
$lang["không"] = "Không";
$lang["add_more"] = "Thêm";
$lang["cây trồng"] = "Cắt";
$lang["thu nhập"] = "Thu nhập";
$lang["income_vs_expenses"] = "Thu nhập vs Chi phí";

$lang["title"] = "Title";
$lang["reset"] = "Reset";
$lang["share_with"] = "Chia sẻ với";
$lang["COMPANY_NAME"] = "Tên công ty";
$lang["địa chỉ"] = "Địa chỉ";
$lang["thành phố"] = "Thành phố";
$lang["nhà nước"] = "Nhà nước";
$lang["zip"] = "Zip";
$lang["Đất nước"] = "Quốc gia";
$lang["điện thoại"] = "Điện thoại";
$lang["private"] = "Private";
$lang["trang web"] = "Trang web";

$lang["chủ nhật"] = "Sunday";
$lang["thứ hai"] = "Monday";
$lang["thứ ba"] = "thứ ba";
$lang["thứ tư"] = "Thứ tư";
$lang["thứ năm"] = "Thứ năm";
$lang["thứ sáu"] = "Friday";
$lang["thứ bảy"] = "Saturday";

$lang["hàng ngày"] = "Hàng ngày";
$lang["hàng tháng"] = "hàng tháng";
$lang["hàng tuần"] = "Hàng tuần";
$lang["hàng năm"] = "hàng năm";

$lang["see_all"] = "Xem tất cả";

/* Thông điệp */
$lang["error_occurred"] = "Xin lỗi, một lỗi xảy ra trong quá trình xử lý các hành động <br /> Vui lòng thử lại sau!.";
$lang["field_required"] = "Trường này là bắt buộc.";
$lang["end_date_must_be_equal_or_greater_than_start_date"] = "Ngày kết thúc phải bằng hoặc lớn hơn Ngày bắt đầu.";
$lang["date_must_be_equal_or_greater_than_today"] = "Ngày phải bằng hoặc lớn hơn ngày hôm nay.";
$lang["enter_valid_email"] = "Vui lòng nhập một địa chỉ email hợp lệ.";
$lang["enter_same_value"] = "Vui lòng nhập giá trị tương tự một lần nữa.";
$lang["record_saved"] = "Biên bản đã được lưu.";
$lang["record_updated"] = "Biên bản đã được cập nhật.";
$lang["record_cannot_be_deleted"] = "Biên bản đang sử dụng, bạn không thể xóa những kỷ lục!";
$lang["record_deleted"] = "Các hồ sơ đã bị xóa.";
$lang["record_undone"] = "Biên bản đã được hoàn tác.";
$lang["settings_updated"] = "Các thiết lập đã được cập nhật.";
$lang["enter_minimum_6_characters"] = "Vui lòng nhập ít nhất 6 ký tự.";
$lang["message_sent"] = "Thông điệp đã được gửi đi.";
$lang["invalid_file_type"] = "Loại tập tin không được phép.";
$lang["something_went_wrong"] = "Rất tiếc, đã xảy ra sai!";
$lang["duplicate_email"] = "Địa chỉ email bạn đã nhập đã được đăng ký.";
$lang["comment_submited"] = "Các bình luận đã được gửi.";
$lang["no_new_messages"] = "Bạn không có tin nhắn mới";
$lang["sent_you_a_message"] = "Đã gửi cho bạn một thông báo";
$lang["max_file_size_3mb_message"] = "Kích thước không nên lớn hơn 3MB";
$lang["keep_it_blank_to_use_default"] = "Giữ nó trống để sử dụng mặc định";
$lang["admin_user_has_all_power"] = "quản lý của người dùng có quyền truy cập / sửa đổi tất cả mọi thứ trong hệ thống này!";
$lang["no_posts_to_show"] = "Không có bài viết để hiển thị";

/* thành viên của đội */
$lang["add_team_member"] = "Thêm thành viên";
$lang["edit_team_member"] = "Chỉnh sửa thành viên trong nhóm";
$lang["delete_team_member"] = "Xóa thành viên trong nhóm";
$lang["team_member"] = "Đội viên";
$lang["team_members"] = "thành viên trong nhóm";
$lang["active_members"] = "thành viên tích cực";
$lang["inactive_members"] = "Các thành viên không hoạt động";
$lang["first_name"] = "First name";
$lang["last_name"] = "Last name";
$lang["mailing_address"] = "Địa chỉ nhận thư";
$lang["alternative_address"] = "Địa chỉ thay thế";
$lang["điện thoại"] = "Điện thoại";
$lang["alternative_phone"] = "điện thoại thay thế";
$lang["giới tính"] = "Giới tính";
$lang["nam"] = "Nam";
$lang["nữ"] = "Nữ";
$lang["date_of_birth"] = "Ngày tháng năm sinh";
$lang["date_of_hire"] = "Ngày thuê";
$lang["ssn"] = "SSN";
$lang["lương"] = "Mức lương";
$lang["salary_term"] = "hạn Mức lương";
$lang["job_info"] = "Thông tin công việc";
$lang["JOB_TITLE"] = "Chức vụ";
$lang["general_info"] = "Thông tin chung";
$lang["ACCOUNT_SETTINGS"] = "Cài đặt tài khoản";
$lang["list_view"] = "Danh sách xem";
$lang["profile_image_changed"] = "Hình ảnh hồ sơ cá nhân đã được thay đổi.";
$lang["send_invitation"] = "Gửi lời mời";
$lang["invitation_sent"] = "Lời mời đã được gửi đi.";
$lang["reset_info_send"] = "Email gửi <br /> Hãy kiểm tra email của bạn để được hướng dẫn!.";
$lang["Hồ sơ"] = "Hồ sơ";
$lang["my_profile"] = "Thông tin cá nhân";
$lang["change_password"] = "Đổi mật khẩu";
$lang["social_links"] = "Liên kết xã hội";
$lang["view_details"] = "Xem chi tiết";
$lang["invite_someone_to_join_as_a_team_member"] = "Mời người nào đó để tham gia như một thành viên trong nhóm.";

/* đội */
$lang["add_team"] = "Thêm nhóm";
$lang["edit_team"] = "Edit đội";
$lang["delete_teamn"] = "Xóa nhóm";
$lang["đội"] = "Team";
$lang["select_a_team"] = "Chọn một đội bóng";

/* bảng điều khiển */
$lang["bảng điều khiển"] = "Bảng điều khiển";

///* Mặt */
$lang["add_attendance"] = "Thêm thời gian bằng tay";
$lang["edit_attendance"] = "Chỉnh sửa thẻ thời gian";
$lang["delete_attendance"] = "Xóa thẻ thời gian";
$lang["tham dự"] = "Thời gian thẻ";
$lang["clock_in"] = "Clock Trong";
$lang["clock_out"] = "Clock Out";
$lang["in_date"] = "Trong ngày";
$lang["out_date"] = "Ngày Out";
$lang["in_time"] = "In Time";
$lang["out_time"] = "Thời gian";
$lang["clock_started_at"] = "Đồng hồ bắt đầu tại";
$lang["you_are_currently_clocked_out"] = "Bạn đang xung nhịp ra";
$lang["members_clocked_in"] = "Viên tốc độ trong";
$lang["members_clocked_out"] = "Các thành viên Clocked Out";
$lang["my_time_cards"] = "thẻ Thời gian của tôi";
$lang["timecard_statistics"] = "Thời gian kê thẻ";
$lang["total_hours_worked"] = "Tổng số giờ làm việc";
$lang["total_project_hours"] = "Tổng số giờ dự án";

///* Loại nghỉ */
$lang["add_leave_type"] = "Thêm loại nghỉ phép";
$lang["edit_leave_type"] = "Edit nghỉ gõ";
$lang["delete_leave_type"] = "Xóa kiểu nghỉ";
$lang["leave_type"] = "Để lại gõ";
$lang["leave_types"] = "Để lại loại";

/* rời khỏi */
$lang["apply_leave"] = "Áp dụng nghỉ phép";
$lang["assign_leave"] = "nghỉ Assign";
$lang["lá"] = "Để lại";
$lang["pending_approval"] = "chờ phê duyệt";
$lang["all_applications"] = "Tất cả các ứng dụng";
$lang["duration"] = "Thời gian";
$lang["single_day"] = "Độc ngày";
$lang["mulitple_days"] = "Nhiều ngày";
$lang["lý do"] = "Reason";
$lang["nộp đơn"] = "nộp đơn";
$lang["phê duyệt"] = "Chấp thuận";
$lang["phê duyệt"] = "Đồng ý";
$lang["từ chối"] = "Bị từ chối";
$lang["từ chối"] = "Từ chối";
$lang["hủy"] = "Hủy";
$lang["hoàn thành"] = "Đã hoàn thành";
$lang["chờ"] = "Đang chờ";
$lang["ngày"] = "Day";
$lang["ngày"] = "Ngày";
$lang["giờ"] = "Hour";
$lang["giờ"] = "Giờ";
$lang["application_details"] = "chi tiết ứng dụng";
$lang["rejected_by"] = "Bị từ chối bởi";
$lang["approved_by"] = "Chấp thuận bởi";
$lang["start_date_to_end_date_format"] = "% s đến% s";
$lang["my_leave"] = "nghỉ của tôi";

/* sự kiện */
$lang["add_event"] = "Thêm sự kiện";
$lang["edit_event"] = "Chỉnh sửa sự kiện";
$lang["delete_event"] = "Xóa sự kiện";
$lang["sự kiện"] = "Sự kiện";
$lang["event_calendar"] = "lịch tổ chức sự kiện";
$lang["location"] = "Vị trí";
$lang["event_details"] = "Chi tiết sự kiện";
$lang["event_deleted"] = "Sự kiện này đã bị xóa.";
$lang["view_on_calendar"] = "Xem trên lịch";
$lang["no_event_found"] = "Không có sự kiện được tìm thấy!";
$lang["events_today"] = "Sự kiện ngày hôm nay";

/* thông báo */
$lang["add_announcement"] = "Thêm thông báo";
$lang["edit_announcement"] = "Chỉnh sửa thông báo";
$lang["delete_announcement"] = "Xóa thông báo";
$lang["thông báo"] = "Thông báo";
$lang["thông báo"] = "Thông báo";
$lang["all_team_members"] = "Tất cả các thành viên trong nhóm";
$lang["all_team_clients"] = "Tất cả các khách hàng";

///* Thiết lập */
$lang["app_settings"] = "Cài đặt ứng dụng";
$lang["APP_TITLE"] = "App Title";
$lang["site_logo"] = "Trang web Logo";
$lang["invoice_logo"] = "Invoice Logo";
$lang["múi giờ"] = "múi giờ";
$lang["date_format"] = "Định dạng ngày";
$lang["time_format"] = "Time Format";
$lang["first_day_of_week"] = "Ngày đầu tiên của Tuần lễ";
$lang["CURRENCY_SYMBOL"] = "tệ Symbol";
$lang["chung"] = "General";
$lang["general_settings"] = "Cài đặt chung";
$lang["item_purchase_code"] = "Item Code mua hàng";
$lang["công ty"] = "Công ty";
$lang["company_settings"] = "Cài đặt Công ty";
$lang["email_settings"] = "Cài đặt Email";
$lang["payment_methods"] = "Phương thức thanh toán";
$lang["email_sent_from_address"] = "Email được gửi từ địa chỉ";
$lang["email_sent_from_name"] = "Email được gửi từ tên";
$lang["email_use_smtp"] = "Sử dụng SMTP";
$lang["email_smtp_host"] = "SMTP Host";
$lang["email_smtp_user"] = "SMTP tài";
$lang["email_smtp_password"] = "SMPT Password";
$lang["email_smtp_port"] = "SMTP Port";
$lang["send_test_mail_to"] = "Gửi một email thử nghiệm để";
$lang["test_mail_sent"] = "Các email kiểm tra đã được gửi đi!";
$lang["test_mail_send_failed"] = "Không thể gửi email thử nghiệm.";
$lang["Cài đặt"] = "Cài đặt";
$lang["cập nhật"] = "Cập nhật";
$lang["current_version"] = "Phiên bản hiện tại";
$lang["ngôn ngữ"] = "Ngôn ngữ";
$lang["ip_restriction"] = "IP Hạn chế";
$lang["varification_failed_message"] = "Xin lỗi, chúng tôi không thể xác minh mã mua hàng của bạn.";
$lang["enter_one_ip_per_line"] = "Nhập một IP trên mỗi dòng Giữ nó trống để cho phép tất cả các IP * Người dùng quản trị sẽ không bị ảnh hưởng...";
$lang["allow_timecard_access_from_these_ips_only"] = "Cho phép truy cập timecard từ chỉ những IP.";
$lang["decimal_separator"] = "Decimal Separator";
$lang["client_settings"] = "Cài đặt Client";
$lang["disable_client_login_and_signup"] = "Disable đăng nhập khách hàng và đăng ký";
$lang["disable_client_login_help_message"] = "địa chỉ liên lạc khách hàng sẽ không thể đăng nhập / đăng ký trong hệ thống này cho đến khi bạn trở lại thiết lập này.";
$lang["who_can_send_or_receive_message_to_or_from_clients"] = "Ai có thể gửi / nhận tin nhắn đến / từ khách hàng";

/* tài khoản */
$lang["authentication_failed"] = "Xác thực không thành công!";
$lang["đăng nhập"] = "Đăng nhập";
$lang["sign_out"] = "Đăng xuất";
$lang["you_dont_have_an_account"] = "Bạn không có tài khoản rồi?";
$lang["already_have_an_account"] = "Bạn đã có tài khoản rồi?";
$lang["forgot_password"] = "Quên mật khẩu?";
$lang["đăng ký"] = "Đăng ký";
$lang["input_email_to_reset_password"] = "Input email của bạn để đặt lại mật khẩu của bạn";
$lang["no_acount_found_with_this_email"] = "Xin lỗi, không tìm thấy tài khoản email này.";
$lang["reset_password"] = "Reset Password";
$lang["password_reset_successfully"] = "Mật khẩu của bạn đã được đặt lại thành công.";
$lang["account_created"] = "Tài khoản của bạn đã được tạo thành công!";
$lang["invitation_expaired_message"] = "Lời mời đã hết hạn hoặc một cái gì đó đã đi sai";
$lang["account_already_exists_for_your_mail"] = "Tài khoản đã tồn tại cho địa chỉ email của bạn.";
$lang["create_an_account_as_a_new_client"] = "Tạo một tài khoản như một khách hàng mới.";
$lang["create_an_account_as_a_team_member"] = "Tạo một tài khoản như một thành viên trong nhóm.";
$lang["create_an_account_as_a_client_contact"] = "Tạo một tài khoản như một số liên lạc của khách hàng.";

///* Thông điệp */
$lang["thông điệp"] = "Tin nhắn";
$lang["thông báo"] = "Message";
$lang["soạn"] = "Soạn thư";
$lang["send_message"] = "Gửi tin nhắn";
$lang["write_a_message"] = "Viết một tin nhắn ...";
$lang["reply_to_sender"] = "Trả lời cho người gửi ...";
$lang["chủ đề"] = "Chủ đề";
$lang["gửi"] = "Gửi";
$lang["để"] = "To";
$lang["từ"] = "Từ";
$lang["hộp thư"] = "Hộp thư đến";
$lang["sent_items"] = "mục Sent";
$lang["tôi"] = "Me";
$lang["select_a_message"] = "Chọn một thông điệp tới xem";

$lang["add_client"] = "Thêm khách hàng";
$lang["edit_client"] = "Edit khách hàng";
$lang["delete_client"] = "Xóa khách hàng";
$lang["khách hàng"] = "Client";
$lang["khách hàng"] = "Khách hàng";
$lang["client_details"] = "chi tiết khách hàng";
$lang["do"] = "Do";

$lang["add_contact"] = "Thêm số liên lạc";
$lang["edit_contact"] = "Chỉnh sửa liên lạc";
$lang["delete_contact"] = "Xóa số liên lạc";
$lang["liên hệ"] = "Liên hệ";
$lang["liên lạc"] = "Danh bạ";
$lang["người sử dụng"] = "Người dùng";
$lang["primary_contact"] = "liên hệ tiểu học";
$lang["disable_login"] = "Disable đăng nhập";
$lang["disable_login_help_message"] = "Người dùng sẽ không thể đăng nhập vào hệ thống này!";
$lang["email_login_details"] = "Email chi tiết đăng nhập để sử dụng này";
$lang["tạo ra"] = "Generate";
$lang["show_text"] = "Hiển thị văn bản";
$lang["hide_text"] = "Ẩn văn bản";
$lang["mark_as_inactive"] = "Đánh dấu là không hoạt động";
$lang["mark_as_inactive_help_message"] = "Người sử dụng không hoạt động sẽ không thể đăng nhập vào hệ thống này và không được tính vào danh sách người dùng hoạt động!";

$lang["invoice_id"] = "Invoice ID";
$lang["thanh toán"] = "Thanh toán";
$lang["invoice_sent_message"] = "Hóa đơn đã được gửi đi!";
$lang["đính kèm"] = "đính kèm";
$lang["VAT_NUMBER"] = "Số thuế GTGT";
$lang["invite_an_user"] = "Mời một người dùng cho% s"; // Mời một người dùng cho {company name}
$lang["UNIT_TYPE"] = "loại Đơn vị";

/* Dự án  */
$lang["add_project"] = "Thêm dự án";
$lang["edit_project"] = "Chỉnh sửa dự án";
$lang["delete_project"] = "Xóa dự án";
$lang["dự án"] = "Dự án";
$lang["dự án"] = "Dự án";
$lang["all_projects"] = "Tất cả các dự án";
$lang["thành viên"] = "Thành viên";
$lang["tổng quan"] = "Tổng quan";
$lang["project_members"] = "Các thành viên của dự án";
$lang["add_member"] = "Thêm thành viên";
$lang["delete_member"] = "Xóa thành viên";
$lang["start_timer"] = "Bắt đầu hẹn giờ";
$lang["stop_timer"] = "Stop hẹn giờ";
$lang["project_timeline"] = "Project Timeline";
$lang["open_projects"] = "Dự án mở";
$lang["projects_completed"] = "Các dự án đã hoàn thành";
$lang["tiến bộ"] = "Progress";
$lang["hoạt động"] = "Hoạt động";
$lang["started_at"] = "Bắt đầu tại";
$lang["customer_feedback"] = "thông tin phản hồi của khách hàng";
$lang["project_comment_reply"] = "bình luận Dự án trả lời";
$lang["task_comment_reply"] = "Task comment trả lời";
$lang["file_comment_reply"] = "File comment trả lời";
$lang["customer_feedback_reply"] = "Ý kiến ​​khách hàng trả lời";

/* Chi phí */
$lang["add_category"] = "Thêm thể loại";
$lang["edit_category"] = "Chỉnh sửa thể loại";
$lang["delete_category"] = "Xóa danh mục";
$lang["thể loại"] = "Thể loại";
$lang["loại"] = "Categories";
$lang["expense_categories"] = "Chi Categories";
$lang["add_expense"] = "Thêm chi phí";
$lang["edit_expense"] = "Chỉnh sửa chi phí";
$lang["delete_expense"] = "Xóa chi phí";
$lang["chi phí"] = "Chi phí";
$lang["chi phí"] = "Chi phí";
$lang["date_of_expense"] = "Ngày chi phí";
$lang["tài chính"] = "Tài chính";

/* Ghi chú */
$lang["add_note"] = "Thêm ghi chú";
$lang["edit_note"] = "Chỉnh sửa chú thích";
$lang["delete_note"] = "Xóa ghi chú";
$lang["lưu ý"] = "Ghi chú";
$lang["ghi chú"] = "Notes";
$lang["sticky_note"] = "Sticky Note (Private)";

/* lịch sử */
$lang["lịch sử"] = "History";

/* thời gian biểu */
$lang["bảng chấm công"] = "bảng chấm công";
$lang["log_time"] = "Đăng thời gian";
$lang["edit_timelog"] = "Chỉnh sửa timelog";
$lang["delete_timelog"] = "Xóa timelog";
$lang["timesheet_statistics"] = "Timesheet kê";

/* Mốc */
$lang["add_milestone"] = "Thêm cột mốc";
$lang["edit_milestone"] = "Chỉnh sửa cột mốc";
$lang["delete_milestone"] = "Xóa cột mốc";
$lang["cột mốc"] = "Milestone";
$lang["cột mốc"] = "cột mốc";

/* các tập tin */
$lang["add_files"] = "Add file";
$lang["edit_file"] = "Chỉnh sửa tập tin";
$lang["delete_file"] = "Xóa tập tin";
$lang["file"] = "File";
$lang["file"] = "Files";
$lang["file_name"] = "File name";
$lang["kích thước"] = "Kích thước";
$lang["uploaded_by"] = "Uploaded by";
$lang["accepted_file_format"] = "định dạng tệp được chấp nhận";
$lang["comma_separated"] = "Comma tách ra";
$lang["project_file"] = "File";
$lang["tải"] = "Tải xuống";
$lang["download_files"] = "Tải file% s"; // Ex. Tải 4 file
$lang["file_preview_is_not_available"] = "File preview không có sẵn.";

/* Nhiệm vụ */
$lang["add_task"] = "Thêm nhiệm vụ";
$lang["edit_task"] = "Chỉnh sửa công việc";
$lang["delete_task"] = "Xóa nhiệm vụ";
$lang["nhiệm vụ"] = "công tác";
$lang["nhiệm vụ"] = "Nhiệm vụ";
$lang["my_tasks"] = "Nhiệm vụ của tôi";
$lang["my_open_tasks"] = "nhiệm vụ mở của tôi";
$lang["assign_to"] = "Gán cho";
$lang["assigned_to"] = "Giao cho";
$lang["nhãn"] = "Nhãn";
$lang["to_do"] = "Để làm được";
$lang["in_progress"] = "Đang thực hiện";
$lang["done"] = "Xong";
$lang["task_info"] = "Task info";
$lang["điểm"] = "điểm";
$lang["điểm"] = "Point";
$lang["task_status"] = "Tình trạng nhiệm vụ";

/* bình luận */
$lang["bình luận"] = "Comment";
$lang["ý kiến"] = "Ý kiến";
$lang["write_a_comment"] = "Viết nhận xét ...";
$lang["write_a_reply"] = "Viết trả lời ...";
$lang["post_comment"] = "Đăng nhận xét";
$lang["post_reply"] = "Đăng trả lời";
$lang["trả lời"] = "Trả lời";
$lang["trả lời"] = "Trả lời";
$lang["like"] = "Like";
$lang["không giống như"] = "Không giống như";
$lang["view"] = "View";
$lang["project_comment"] = "Dự án Bình luận";
$lang["task_comment"] = "Task Comment";
$lang["file_comment"] = "File Comment";

/* Định dạng thời gian */
$lang["hôm nay"] = "Hôm nay";
$lang["ngày hôm qua"] = "Hôm qua";
$lang["ngày mai"] = "Ngày mai";

$lang["today_at"] = "Hôm nay lúc";
$lang["yesterday_at"] = "Hôm qua lúc";

/* vé */

$lang["add_ticket"] = "Add vé";
$lang["vé"] = "vé";
$lang["vé"] = "Vé";
$lang["TICKET_ID"] = "Ticket ID";
$lang["client_replied"] = "Khách hàng trả lời:";
$lang["change_status"] = "Thay đổi trạng thái";
$lang["last_activity"] = "Hoạt động lần cuối";
$lang["open_tickets"] = "Open vé";
$lang["ticket_status"] = "Tình trạng vé";

/* Loại vé */

$lang["add_ticket_type"] = "Thêm loại vé";
$lang["ticket_type"] = "loại vé";
$lang["ticket_types"] = "loại vé";
$lang["edit_ticket_type"] = "loại vé Chỉnh sửa";
$lang["delete_ticket_type"] = "Delete loại vé";

/* Phương thức thanh toán */

$lang["add_payment_method"] = "Thêm phương thức thanh toán";
$lang["PAYMENT_METHOD"] = "Phương thức thanh toán";
$lang["payment_methods"] = "Phương thức thanh toán";
$lang["edit_payment_method"] = "phương pháp Chỉnh sửa thanh toán";
$lang["delete_payment_method"] = "Xóa phương thức thanh toán";

/* Hoá đơn */

$lang["add_invoice"] = "Add hóa đơn";
$lang["edit_invoice"] = "Chỉnh sửa hóa đơn";
$lang["delete_invoice"] = "Xóa hóa đơn";
$lang["hóa đơn"] = "Hóa đơn";
$lang["hóa đơn"] = "Hóa đơn";
$lang["bill_date"] = "ngày Bill";
$lang["DUE_DATE"] = "Do ngày";
$lang["PAYMENT_DATE"] = "Ngày thanh toán";
$lang["bill_to"] = "Bill To";
$lang["invoice_value"] = "Hóa đơn giá trị gia tăng";
$lang["payment_received"] = "Thanh toán nhận";
$lang["invoice_payments"] = "Thanh toán";
$lang["dự thảo"] = "Nháp";
$lang["fully_paid"] = "Fully trả";
$lang["partially_paid"] = "Một phần trả";
$lang["not_paid"] = "Không trả";
$lang["quá hạn"] = "quá hạn";
$lang["invoice_items"] = "mục Invoice";
$lang["edit_invoice"] = "Chỉnh sửa hóa đơn";
$lang["delete_invoice"] = "Xóa hóa đơn";
$lang["item"] = "Item";
$lang["add_item"] = "Thêm mục";
$lang["create_new_item"] = "Tạo mục mới";
$lang["select_or_create_new_item"] = "Chọn từ danh sách hoặc tạo mục mới ...";
$lang["số lượng"] = "Số lượng";
$lang["tỷ lệ"] = "Tỷ lệ";
$lang["total_of_all_pages"] = "Tổng số của tất cả các trang";
$lang["sub_total"] = "Sub Tổng số";
$lang["tổng"] = "Tổng số";
$lang["last_email_sent"] = "email cuối gửi";
$lang["item_library"] = "Item thư viện";
$lang["add_payment"] = "Add thanh toán";
$lang["không bao giờ"] = "Không bao giờ";
$lang["email_invoice_to_client"] = "hóa đơn qua email cho khách hàng";
$lang["download_pdf"] = "Download PDF";
$lang["print"] = "In";
$lang["hành động"] = "Hành động";
$lang["balance_due"] = "Số tiền còn thiếu";
$lang["trả"] = "Paid";
$lang["lượng"] = "Số tiền";
$lang["invoice_payment_list"] = "danh sách thanh toán hóa đơn";
$lang["invoice_statistics"] = "Hóa đơn kê";
$lang["thanh toán"] = "Thanh toán";

/* mẫu thư điện tử */
$lang["email_templates"] = "mẫu Email";
$lang["select_a_template"] = "Chọn một mẫu để chỉnh sửa";
$lang["avilable_variables"] = "biến sẵn";
$lang["restore_to_default"] = "Restore để mặc định";
$lang["template_restored"] = "Mẫu đã được khôi phục để mặc định.";
$lang["login_info"] = "Thông tin Đăng nhập";
$lang["reset_password"] = "Reset password";
$lang["team_member_invitation"] = "lời mời đội viên";
$lang["client_contact_invitation"] = "liên hệ mời khách hàng";
$lang["send_invoice"] = "Gửi hóa đơn";
$lang["chữ ký"] = "Chữ ký";

/* Vai trò */

$lang["vai trò"] = "Vai trò";
$lang["vai trò"] = "Vai trò";
$lang["add_role"] = "Add vai trò";
$lang["edit_role"] = "Chỉnh sửa vai trò";
$lang["delete_role"] = "Xóa vai trò";
$lang["use_seetings_from"] = "Sử dụng cài đặt từ";
$lang["quyền"] = "Quyền";
$lang["yes_all_members"] = "Vâng, tất cả các thành viên";
$lang["yes_specific_members_or_teams"] = "Vâng, thành viên hoặc nhóm cụ thể";
$lang["yes_specific_ticket_types"] = "Vâng, các loại vé cụ thể";
$lang["select_a_role"] = "Chọn một vai trò";
$lang["choose_members_and_or_teams"] = "Chọn các thành viên và / hoặc đội";
$lang["choose_ticket_types"] = "Chọn loại vé";
$lang["excluding_his_her_time_cards"] = "Trừ / thẻ thời gian riêng của mình";
$lang["excluding_his_her_leaves"] = "Trừ / lá của chính mình";
$lang["can_manage_team_members_leave"] = "Có thể quản lý lá thành viên trong nhóm không?";
$lang["can_manage_team_members_timecards"] = "Có thể quản lý thẻ thời gian thành viên trong nhóm không?";
$lang["can_access_invoices"] = "Có thể truy cập hóa đơn?";
$lang["can_access_expenses"] = "có thể truy cập vào chi phí?";
$lang["can_access_clients_information"] = "có thể truy cập thông tin của khách hàng?";
$lang["can_access_tickets"] = "Có thể truy cập vé?";
$lang["can_manage_announcements"] = "Có thể quản lý các thông báo?";

/* mốc thời gian */
$lang["post_placeholder_text"] = "Chia sẻ một ý tưởng hay tài liệu ...";
$lang["bài"] = "Post";
$lang["timeline"] = "Timeline";
$lang["load_more"] = "Tải thêm";
$lang["upload_file"] = "Upload File";
$lang["upload"] = "Tải lên";
$lang["new_posts"] = "bài viết mới";

/* Thuế */

$lang["add_tax"] = "Add thuế";
$lang["thuế"] = "THUẾ";
$lang["thuế"] = "Thuế";
$lang["edit_tax"] = "Edit thuế";
$lang["delete_tax"] = "Xóa thuế";
$lang["tỷ lệ"] = "Tỷ lệ phần trăm (%)";
$lang["second_tax"] = "Second THUẾ";

/* Version 1.2 */
$lang["available_on_invoice"] = "Khả dụng trên hóa đơn";
$lang["available_on_invoice_help_text"] = "Các phương thức thanh toán sẽ được xuất hiện trong hóa đơn của khách hàng.";
$lang["minimum_payment_amount"] = "số tiền thanh toán tối thiểu";
$lang["minimum_payment_amount_help_text"] = "Khách hàng sẽ không có khả năng thanh toán hóa đơn sử dụng phương thức thanh toán này, nếu giá trị hóa đơn thấp hơn giá trị này.";
$lang["pay_invoice"] = "Pay Invoice";
$lang["pay_button_text"] = "nút Pay văn bản";
$lang["minimum_payment_validation_message"] = "Số tiền thanh toán không thể thấp sau đó:"; // cũ. Số tiền thanh toán không thể ít thì: 100.00 USD
$lang["invoice_settings"] = "Hóa đơn Settings";
$lang["allow_partial_invoice_payment_from_clients"] = "Cho phép thanh toán một phần từ các khách hàng";
$lang["invoice_color"] = "Invoice Color";
$lang["invoice_footer"] = "Invoice Footer";
$lang["invoice_preview"] = "Invoice Preview";
$lang["close_preview"] = "Close Preview";
$lang["only_me"] = "Chỉ có tôi";
$lang["specific_members_and_teams"] = "Các thành viên và các đội cụ thể";
$lang["rows_per_page"] = "Số hàng mỗi trang";
$lang["giá"] = "Giá";
$lang["SECURITY_TYPE"] = "Loại Security";

$lang["client_can_view_tasks"] = "Khách hàng có thể xem nhiệm vụ?";
$lang["client_can_create_tasks"] = "Khách hàng có thể tạo ra nhiệm vụ?";
$lang["client_can_edit_tasks"] = "Khách hàng có thể chỉnh sửa nhiệm vụ?";
$lang["client_can_comment_on_tasks"] = "Khách hàng có thể nhận xét về nhiệm vụ?";

$lang["set_project_permissions"] = "Thiết lập quyền dự án";
$lang["can_create_projects"] = "có thể tạo ra các dự án";
$lang["can_edit_projects"] = "Có thể chỉnh sửa dự án";
$lang["can_delete_projects"] = "thể xóa dự án";
$lang["can_create_tasks"] = "có thể tạo ra nhiệm vụ";
$lang["can_edit_tasks"] = "Có thể chỉnh sửa nhiệm vụ";
$lang["can_delete_tasks"] = "thể xóa nhiệm vụ";
$lang["can_comment_on_tasks"] = "Có thể nhận xét về nhiệm vụ";
$lang["can_create_milestones"] = "có thể tạo ra những cột mốc";
$lang["can_edit_milestones"] = "Có thể chỉnh sửa sự kiện quan trọng";
$lang["can_delete_milestones"] = "thể xóa cột mốc";
$lang["can_add_remove_project_members"] = "Có thể thêm / xóa các thành viên dự án";
$lang["can_delete_files"] = "có thể xóa các tập tin";

/* Version 1.2.2 */
$lang["label"] = "Label";
$lang["send_bcc_to"] = "Khi gửi hóa đơn cho khách hàng, gửi BCC để";
$lang["mark_project_as_completed"] = "Mark Dự án khi hoàn tất";
$lang["mark_project_as_canceled"] = "Mark dự án như đã bị hủy";
$lang["mark_project_as_open"] = "Mark dự án như mở";

/* Phiên bản 1.3 */
$lang["thông báo"] = "Thông báo";
$lang["thông báo"] = "Thông báo";
$lang["notification_settings"] = "Cài đặt thông báo";
$lang["enable_email"] = "Enable email";
$lang["enable_web"] = "Enable web";
$lang["sự kiện"] = "Sự kiện";
$lang["notify_to"] = "Thông báo để";

$lang["project_created"] = "Dự án tạo";
$lang["project_deleted"] = "Dự án xóa";
$lang["project_task_created"] = "nhiệm vụ dự án tạo";
$lang["project_task_updated"] = "nhiệm vụ Dự án cập nhật";
$lang["project_task_assigned"] = "nhiệm vụ dự án giao";
$lang["project_task_started"] = "nhiệm vụ Dự án bắt đầu";
$lang["project_task_finished"] = "nhiệm vụ Dự án đã hoàn thành";
$lang["project_task_reopened"] = "nhiệm vụ dự án mở cửa trở lại";
$lang["project_task_deleted"] = "nhiệm vụ Dự án xóa";
$lang["project_task_commented"] = "nhiệm vụ dự án nhận xét";
$lang["project_member_added"] = "Thành viên dự án bổ sung";
$lang["project_member_deleted"] = "thành viên Dự án xóa";
$lang["project_file_added"] = "tập tin dự án bổ sung";
$lang["project_file_deleted"] = "tập tin dự án xóa";
$lang["project_file_commented"] = "tập tin dự án nhận xét";
$lang["project_comment_added"] = "bình luận Dự án bổ sung";
$lang["project_comment_replied"] = "bình luận Dự án trả lời";
$lang["project_customer_feedback_added"] = "Dự án thông tin phản hồi của khách hàng thêm";
$lang["project_customer_feedback_replied"] = "Dự án thông tin phản hồi của khách hàng trả lời:";
$lang["client_signup"] = "Khách hàng đăng ký";
$lang["invoice_online_payment_received"] = "Thanh toán trực tuyến hóa đơn nhận được";
$lang["leave_application_submitted"] = "Rời khỏi ứng dụng nộp";
$lang["leave_approved"] = "Để lại chấp thuận";
$lang["leave_assigned"] = "Rời khỏi giao";
$lang["leave_rejected"] = "Để lại từ chối";
$lang["leave_canceled"] = "Rời khỏi bị hủy bỏ";
$lang["ticket_created"] = "Ticket tạo";
$lang["ticket_commented"] = "Ticket nhận xét";
$lang["ticket_closed"] = "vé đóng cửa";
$lang["ticket_reopened"] = "vé mở cửa trở lại";
$lang["rời"] = "Để lại";

$lang["client_primary_contact"] = "liên hệ chính của khách hàng";
$lang["client_all_contacts"] = "Tất cả các địa chỉ liên lạc của khách hàng";
$lang["task_assignee"] = "Task nhận chuyển nhượng";
$lang["task_collaborators"] = "cộng tác viên công tác";
$lang["comment_creator"] = "Bình luận sáng tạo";
$lang["leave_applicant"] = "Rời khỏi ứng viên";
$lang["ticket_creator"] = "tác giả vé";

$lang["no_new_notifications"] = "Không thông báo tìm thấy.";

/* Thư thông báo */

$lang["notification_project_created"] = "Tạo một dự án mới.";
$lang["notification_project_deleted"] = "Deleted một dự án.";
$lang["notification_project_task_created"] = "Tạo một nhiệm vụ mới.";
$lang["notification_project_task_updated"] = "Cập nhật một nhiệm vụ.";
$lang["notification_project_task_assigned"] = "giao một nhiệm vụ đến% s"; // giao một nhiệm vụ cho ông X
$lang["notification_project_task_started"] = "Bắt đầu một công việc.";
$lang["notification_project_task_finished"] = "Xong một nhiệm vụ.";
$lang["notification_project_task_reopened"] = "mở lại một nhiệm vụ.";
$lang["notification_project_task_deleted"] = "Deleted một nhiệm vụ.";
$lang["notification_project_task_commented"] = "nhận xét về một nhiệm vụ.";
$lang["notification_project_member_added"] = "thêm% s trong một dự án."; // Added Ông X trong một dự án.
$lang["notification_project_member_deleted"] = "Deleted% s từ một dự án."; // xóa Ông X từ một dự án.
$lang["notification_project_file_added"] = "Thêm một tập tin trong dự án.";
$lang["notification_project_file_deleted"] = "Deleted một tập tin từ dự án.";
$lang["notification_project_file_commented"] = "nhận xét về một tập tin.";
$lang["notification_project_comment_added"] = "nhận xét về một dự án.";
$lang["notification_project_comment_replied"] = "trả lời trên một bình luận dự án.";
$lang["notification_project_customer_feedback_added"] = "nhận xét về một dự án.";
$lang["notification_project_customer_feedback_replied"] = "trả lời trên nhận xét.";
$lang["notification_client_signup"] = "Đã đăng ký như một khách hàng mới."; //Ông. X đăng ký như một khách hàng mới.
$lang["notification_invoice_online_payment_received"] = "Đăng thanh toán trực tuyến.";
$lang["notification_leave_application_submitted"] = "Đăng một ứng dụng nghỉ phép.";
$lang["notification_leave_approved"] = "Phê duyệt nghỉ của% s."; // Phê duyệt nghỉ của ông X
$lang["notification_leave_assigned"] = "Assigned nghỉ đến% s."; // được chỉ định nghỉ ông X
$lang["notification_leave_rejected"] = "Bị từ chối nghỉ% s."; // Phê duyệt nghỉ của ông X
$lang["notification_leave_canceled"] = "hủy một appliction nghỉ.";
$lang["notification_ticket_created"] = "Tạo một thẻ mới.";
$lang["notification_ticket_commented"] = "nhận xét về một vé.";
$lang["notification_ticket_closed"] = "Closed vé.";
$lang["notification_ticket_reopened"] = "mở lại vé.";

$lang["general_notification"] = "Thông báo chung";

$lang["disable_online_payment"] = "Disable thanh toán trực tuyến";
$lang["disable_online_payment_description"] = "Ẩn các tùy chọn thanh toán trực tuyến trong hóa đơn cho khách hàng này.";

$lang["client_can_view_project_files"] = "Khách hàng có thể xem các tập tin dự án?";
$lang["client_can_add_project_files"] = "Khách hàng có thể thêm các tập tin dự án?";
$lang["client_can_comment_on_files"] = "Khách hàng có thể bình luận của file?";
$lang["mark_invoice_as_not_paid"] = "Đánh dấu là Không trả"; // Thay đổi trạng thái hóa đơn để Không trả tiền

$lang["set_team_members_permission"] = "Đặt thành viên trong nhóm quyền";
$lang["can_view_team_members_contact_info"] = "Có thể xem thông tin liên hệ thành viên trong nhóm không?";
$lang["can_view_team_members_social_links"] = "Có thể xem liên kết xã hội thành viên trong nhóm không?";

$lang["cộng tác viên"] = "Cộng tác viên";
$lang["cộng tác viên"] = "Cộng tác viên";

/* Phiên bản 1.4 */

$lang["module"] = "module";
$lang["manage_modules"] = "Quản lý Modules";
$lang["module_settings_instructions"] = "Chọn module bạn muốn sử dụng.";

$lang["task_point_help_text"] = "Task điểm coi là một giá trị công việc Bạn có thể đặt 5 điểm cho các nhiệm vụ rất khó khăn và 1 điểm cho các nhiệm vụ dễ dàng.."; // ý nghĩa của điểm nhiệm vụ

$lang["mark_as_open"] = "Đánh dấu là Open";
$lang["mark_as_closed"] = "Đánh dấu là Closed";

$lang["ticket_assignee"] = "Ticket nhận chuyển nhượng";

$lang["ước tính"] = "Ước tính";
$lang["ước tính"] = "Ước tính";
$lang["estimate_request"] = "Ước tính Yêu cầu";
$lang["estimate_requests"] = "Yêu cầu Ước tính";
$lang["estimate_list"] = "Ước List";
$lang["estimate_forms"] = "Ước tính hình thức";
$lang["estimate_request_forms"] = "Ước tính Yêu cầu hình thức";

$lang["add_form"] = "Thêm hình thức";
$lang["edit_form"] = "Chỉnh sửa hình thức";
$lang["delete_form"] = "Xóa hình thức";

$lang["add_field"] = "Add trường";
$lang["giữ chỗ"] = "giữ chỗ";
$lang["yêu cầu"] = "Yêu cầu";

$lang["FIELD_TYPE"] = "Field Loại";
$lang["xem trước"] = "Xem trước";

$lang["field_type_text"] = "Text";
$lang["field_type_textarea"] = "Textarea";
$lang["field_type_select"] = "Chọn";
$lang["field_type_multi_select"] = "Multi Chọn";

$lang["request_an_estimate"] = "Yêu cầu Ước tính";
$lang["estimate_submission_message"] = "Yêu cầu của bạn đã được gửi thành công!";

$lang["giữ"] = "Giữ";
$lang["chế biến"] = "chế biến";
$lang["ước tính"] = "ước tính";

$lang["add_estimate"] = "Add ước tính";
$lang["edit_estimate"] = "Chỉnh sửa dự toán";
$lang["delete_estimate"] = "Xóa dự toán";
$lang["valid_until"] = "Có giá trị đến";
$lang["estimate_date"] = "ngày Estimate";
$lang["chấp nhận"] = "Đã chấp nhận";
$lang["giảm"] = "bị từ chối";
$lang["gửi"] = "Sent";
$lang["estimate_preview"] = "Ước tính Preview";
$lang["estimate_to"] = "Estimate Để";

$lang["can_access_estimates"] = "có thể truy cập vào dự toán?";
$lang["request_an_estimate"] = "Yêu cầu ước tính";
$lang["estimate_request_form_selection_title"] = "Hãy chọn một mẫu từ danh sách sau đây để gửi yêu cầu của bạn.";

$lang["mark_as_processing"] = "Đánh dấu là chế biến";
$lang["mark_as_estimated"] = "Đánh dấu là ước tính";
$lang["mark_as_hold"] = "Đánh dấu là Hold";
$lang["mark_as_canceled"] = "Đánh dấu là đã bị hủy";

$lang["mark_as_sent"] = "Đánh dấu là Sent";
$lang["mark_as_accepted"] = "Đánh dấu là chấp nhận";
$lang["mark_as_rejected"] = "Đánh dấu là bị từ chối";
$lang["mark_as_declined"] = "Đánh dấu là bị từ chối";

$lang["estimate_request_received"] = "Yêu cầu Ước tính đã nhận được";
$lang["estimate_sent"] = "Estimate gửi";
$lang["estimate_accepted"] = "Ước tính chấp nhận";
$lang["estimate_rejected"] = "Ước tính từ chối";

$lang["notification_estimate_request_received"] = "Đăng một yêu cầu dự toán";
$lang["notification_estimate_sent"] = "Sent ước tính";
$lang["notification_estimate_accepted"] = "Được chấp nhận ước tính";
$lang["notification_estimate_rejected"] = "Bị từ chối ước tính";

$lang["clone_project"] = "Clone Dự án";
$lang["copy_tasks"] = "Copy nhiệm vụ";
$lang["copy_project_members"] = "Copy dự án thành viên";
$lang["copy_milestones"] = "Sao chép sự kiện quan trọng";
$lang["copy_same_assignee_and_collaborators"] = "Sao chép cùng nhận chuyển nhượng và cộng tác viên";
$lang["copy_tasks_start_date_and_deadline"] = "Copy nhiệm vụ ngày bắt đầu và thời hạn";
$lang["task_comments_will_not_be_included"] = "Nhiệm vụ bình luận sẽ không được đưa";
$lang["project_cloned_successfully"] = "Dự án đã được nhân bản thành công";

$lang["tìm kiếm"] = "Search";
$lang["no_record_found"] = "Không có hồ sơ được tìm thấy.";
$lang["excel"] = "Excel";
$lang["print_button_help_text"] = "Nhấn phím escape khi hoàn tất.";
$lang["are_you_sure"] = "Bạn có chắc chắn?";
$lang["file_upload_instruction"] = "Kéo và thả các tài liệu ở đây <br /> (hoặc click để duyệt ...)";
$lang["file_name_too_long"] = "Tên tệp quá dài.";
$lang["cuộn"] = "Scrollbar";

$lang["short_sunday"] = "Sun";
$lang["short_monday"] = "Mon";
$lang["short_tuesday"] = "Tue";
$lang["short_wednesday"] = "Wed";
$lang["short_thursday"] = "Thu";
$lang["short_friday"] = "Fri";
$lang["short_saturday"] = "Sat";

$lang["min_sunday"] = "Su";
$lang["min_monday"] = "Mo";
$lang["min_tuesday"] = "Tu";
$lang["min_wednesday"] = "Chúng tôi";
$lang["min_thursday"] = "Th";
$lang["min_friday"] = "cha";
$lang["min_saturday"] = "Sa";

$lang["tháng một"] = "tháng một";
$lang["tháng hai"] = "tháng hai";
$lang["diễu hành"] = "March";
$lang["tháng tư"] = "April";
$lang["có thể"] = "tháng";
$lang["tháng sáu"] = "June";
$lang["Tháng bảy"] = "Tháng bảy";
$lang["tháng tám"] = "August";
$lang["Tháng chín"] = "Tháng Chín";
$lang["Tháng Mười"] = "Tháng Mười";
$lang["Tháng Mười Một"] = "November";
$lang["Tháng mười hai"] = "December";

$lang["short_january"] = "Jan";
$lang["short_february"] = "tháng hai";
$lang["short_march"] = "Mar";
$lang["short_april"] = "tháng tư";
$lang["short_may"] = "tháng";
$lang["short_june"] = "Jun";
$lang["short_july"] = "Tháng bảy";
$lang["short_august"] = "tháng tám";
$lang["short_september"] = "Tháng chín";
$lang["short_october"] = "Tháng Mười";
$lang["short_november"] = "Tháng mười một";
$lang["short_december"] = "Tháng mười hai";

/* Phiên bản 1.5 */

$lang["no_such_file_or_directory_found"] = "Không có tập tin hoặc thư mục đó.";
$lang["Gantt"] = "Gantt";
$lang["not_specified"] = "Không xác định";
$lang["group_by"] = "Nhóm theo";
$lang["create_invoice"] = "Tạo hóa đơn";
$lang["include_all_items_of_this_estimate"] = "Bao gồm tất cả các mục của ước tính này";
$lang["edit_payment"] = "Chỉnh sửa thanh toán";
$lang["disable_client_login"] = "Disable khách hàng đăng nhập";
$lang["disable_client_signup"] = "Vô hiệu hoá đăng ký khách hàng";

$lang["biểu đồ"] = "Chart";
$lang["signin_page_background"] = "| Đăng nhập nền trang";
$lang["show_logo_in_signin_page"] = "Hiển thị biểu tượng trong trang đăng nhập";
$lang["show_background_image_in_signin_page"] = "image Hiện nền trong trang đăng nhập";

/* Phiên bản 1.6 */

$lang["nhiều hơn"] = "More";
$lang["tùy chỉnh"] = "Custom";
$lang["rõ ràng"] = "Clear";
$lang["hết hạn"] = "hết hạn";
$lang["enable_attachment"] = "Enable tập tin đính kèm";
$lang["custom_fields"] = "Trường tùy chỉnh";
$lang["edit_field"] = "Chỉnh sửa trường";
$lang["delete_field"] = "Xóa trường";
$lang["client_info"] = "Thông tin khách hàng";
$lang["edit_expenses_category"] = "Chỉnh sửa chi phí loại";
$lang["eelete_expenses_category"] = "Xóa danh mục chi phí";
$lang["empty_starred_projects"] = "Để truy cập vào các dự án yêu thích của bạn một cách nhanh chóng, hãy truy cập vào xem dự án và đánh dấu sao.";
$lang["empty_starred_clients"] = "Để truy cập vào các khách hàng yêu thích của bạn một cách nhanh chóng, hãy truy cập vào quan điểm của khách hàng và đánh dấu sao.";
$lang["download_zip_name"] = "văn bản";
$lang["invoice_prefix"] = "Hóa đơn tiền tố";
$lang["invoice_style"] = "Invoice phong cách";
$lang["delete_confirmation_message"] = "Bạn có chắc chắn bạn sẽ không thể hoàn tác hành động này?!";
$lang["left"] = "Left";
$lang["đúng"] = "Right";
$lang["currency_position"] = "tệ Chức vụ";
$lang["người nhận"] = "Người nhận";

$lang["new_message_sent"] = "Tin nhắn mới gửi";
$lang["message_reply_sent"] = "Thông điệp trả lời";
$lang["notification_new_message_sent"] = "Đã gửi một thông điệp.";
$lang["notification_message_reply_sent"] = "trả lời một tin nhắn.";
$lang["invoice_payment_confirmation"] = "xác nhận thanh toán hóa đơn";
$lang["notification_invoice_payment_confirmation"] = "Số tiền đã nhận";

/* Phiên bản 1.7 */

$lang["client_can_create_projects"] = "Khách hàng có thể tạo các dự án?";
$lang["client_can_view_timesheet"] = "Khách hàng có thể xem timesheet?";
$lang["client_can_view_gantt"] = "Khách hàng có thể xem Gantt?";
$lang["client_can_view_overview"] = "Khách hàng có thể xem tổng quan về dự án?";
$lang["client_can_view_milestones"] = "Khách hàng có thể xem sự kiện quan trọng?";

$lang["items"] = "Items";
$lang["edit_item"] = "Chỉnh sửa mục";
$lang["item_edit_instruction"] = "Lưu ý: Các thay đổi này sẽ không bị ảnh hưởng trên hoá đơn hoặc ước tính hiện có.";

$lang["định kỳ"] = "định kỳ";
$lang["repeat_every"] = "Lặp lại mỗi"; // Ex. lặp lại mỗi 2 tháng
$lang["interval_days"] = "Day (s)";
$lang["interval_weeks"] = "Tuần (s)";
$lang["interval_months"] = "Tháng (s)";
$lang["interval_years"] = "Năm (s)";
$lang["chu kỳ"] = "Cycles";
$lang["recurring_cycle_instructions"] = "lặp đi lặp lại sẽ được dừng lại sau khi số chu kỳ Giữ nó trống cho vô cùng..";
$lang["next_recurring_date"] = "Next định kỳ";
$lang["dừng lại"] = "Ngưng";
$lang["past_recurring_date_error_message_title"] = "Ngày hóa đơn được lựa chọn và lặp lại kiểu trả về một ngày trong quá khứ.";
$lang["past_recurring_date_error_message"] = "ngày lặp đi lặp lại Tiếp theo phải là một ngày trong tương lai Vui lòng nhập một ngày trong tương lai..";
$lang["sub_invoices"] = "hoá đơn Sub";

$lang["cron_job_required"] = "Cron Job là cần thiết cho hành động này!";

$lang["recurring_invoice_created_vai_cron_job"] = "hóa đơn định kỳ tạo ra thông qua Cron Job";
$lang["notification_recurring_invoice_created_vai_cron_job"] = "hóa đơn mới được tạo ra";

$lang["field_type_number"] = "Số";
$lang["show_in_table"] = "Hiển thị trong bảng";
$lang["show_in_invoice"] = "Hiển thị trong hóa đơn";
$lang["visible_to_admins_only"] = "Hiển thị đối với quản trị viên chỉ";
$lang["hide_from_clients"] = "Ẩn từ khách hàng";
$lang["công cộng"] = "công cộng";

$lang["giúp đỡ"] = "Trợ giúp";
$lang["bài báo"] = "bài viết";
$lang["add_article"] = "Thêm bài viết mới";
$lang["edit_article"] = "Chỉnh sửa bài viết";
$lang["delete_article"] = "Xóa bài viết";
$lang["can_manage_help_and_knowledge_base"] = "Có thể quản lý giúp đỡ và nền tảng kiến ​​thức?";

$lang["how_can_we_help"] = "Làm thế nào chúng ta có thể giúp đỡ?";
$lang["help_page_title"] = "Internal Wiki";
$lang["search_your_question"] = "Tìm kiếm câu hỏi của bạn";
$lang["no_result_found"] = "Không có kết quả được tìm thấy.";
$lang["loại"] = "Sắp xếp";
$lang["total_views"] = "Tổng quan";

$lang["help_and_support"] = "Help & Support";
$lang["knowledge_base"] = "Kiến thức cơ bản";

$lang["payment_success_message"] = "Thanh toán của bạn đã được hoàn thành.";
$lang["payment_card_charged_but_system_error_message"] = "Bạn có thể phải trả thẻ nhưng chúng tôi không thể hoàn tất quá trình Vui lòng liên hệ tới admin hệ thống của bạn.";
$lang["card_payment_failed_error_message"] = "Chúng tôi không thể xử lý thanh toán của bạn ngay bây giờ, vì vậy xin vui lòng thử lại sau.";

$lang["message_received"] = "Thông điệp nhận được";
$lang["in_number_of_days"] = "Trong ngày% s"; // Ex. Trong 7 ngày
$lang["chi tiết"] = "Details";
$lang["Tóm tắt"] = "Summary";
$lang["project_timesheet"] = "timesheet Dự án";

$lang["set_event_permissions"] = "Thiết lập quyền kiện";
$lang["disable_event_sharing"] = "Vô hiệu hoá sự kiện chia sẻ";
$lang["can_update_team_members_general_info_and_social_links"] = "có thể cập nhật thông tin nói chung và liên kết xã hội nhóm thành viên không?";
$lang["can_manage_team_members_project_timesheet"] = "Có thể quản lý timesheet dự án thành viên trong nhóm không?";

$lang["cron_job"] = "Cron Job";
$lang["cron_job_link"] = "liên kết Cron Job" ;
$lang["last_cron_job_run"] = "Last Cron Job chạy";
$lang["created_from"] = "Tạo từ"; // Ex. Tạo ra từ Invoice # 1
$lang["recommended_execution_interval"] = "Recommended khoảng thực hiện";

/* Version 1.8 */

$lang["hội nhập"] = "hội nhập";
$lang["get_your_key_from_here"] = "Nhận chìa khóa của bạn từ đây:";
$lang["re_captcha_site_key"] = "chìa khóa trang web";
$lang["re_captcha_secret_key"] = "chìa khóa bí mật";

$lang["re_captcha_error-thiếu-input-bí mật"] = "reCAPTCHA bí mật là mất tích";
$lang["re_captcha_error-invalid-input-bí mật"] = "reCAPTCHA bí mật là không hợp lệ.";
$lang["re_captcha_error-thiếu-input-response"] = "Hãy chọn reCAPTCHA.";
$lang["re_captcha_error-invalid-input-response"] = "Tham số phản ứng không hợp lệ hoặc bị thay đổi.";
$lang["re_captcha_error-xấu-yêu cầu"] = "Yêu cầu không hợp lệ hoặc bị thay đổi.";
$lang["re_captcha_expired"] = "Các reCAPTCHA đã hết hạn Hãy tải lại trang..";

$lang["yes_all_tickets"] = "Vâng, tất cả vé";
$lang["choose_ticket_types"] = "Chọn loại vé";

$lang["can_manage_all_projects"] = "Có thể quản lý tất cả các dự án";
$lang["show_most_recent_ticket_comments_at_the_top"] = "Hiển thị nhận xét vé gần đây nhất ở đầu";

$lang["new_event_added_in_calendar"] = "sự kiện mới được thêm vào lịch";
$lang["notification_new_event_added_in_calendar"] = "Thêm một sự kiện mới.";

$lang["todo"] = "Để làm được";
$lang["add_a_todo"] = "Thêm một để làm ...";

/* Version 1.9 */

$lang["client_groups"] = "nhóm khách hàng";
$lang["add_client_group"] = "Thêm nhóm khách hàng";
$lang["edit_client_group"] = "Chỉnh sửa nhóm khách hàng";
$lang["delete_client_group"] = "Xóa nhóm khách hàng";

$lang["ticket_prefix"] = "Ticket tiền tố";
$lang["add_a_task"] = "Thêm một nhiệm vụ ...";

$lang["add_task_status"] = "Thêm trạng thái nhiệm vụ";
$lang["edit_task_status"] = "tình trạng Sửa nhiệm vụ";
$lang["delete_task_status"] = "Xóa trạng thái công việc";

$lang["danh sách"] = "Danh sách";
$lang["Kanban"] = "Kanban";
$lang["ưu tiên"] = "ưu tiên";
$lang["moved_up"] = "Moved Up";
$lang["moved_down"] = "Đã chuyển xuống";
$lang["mark_project_as_hold"] = "Mark dự án như Giữ";

$lang["lặp lại"] = "Lặp lại";

$lang["hide_team_members_list"] = "thành viên trong nhóm Ẩn danh?";
