<div class="table-responsive">
    <table id="income-vs-expenses-summary-table" class="display" cellspacing="0" width="100%">
        <tfoot>
            <tr>
                <th colspan="2" class="text-right"><?php echo lang("total") ?>:</th>
                <th class="text-right" data-current-page="2"></th>
                <th class="text-right" data-current-page="3"></th>
                <th class="text-right" data-current-page="4"></th>
            </tr>
            <tr data-section="all_pages">
                <th colspan="2" class="text-right"><?php echo lang("total_of_all_pages") ?>:</th>
                <th class="text-right" data-all-page="2"></th>
                <th class="text-right" data-all-page="3"></th>
                <th class="text-right" data-all-page="4"></th>
            </tr>
        </tfoot>
    </table>
</div>

<script>
    $("#income-vs-expenses-summary-table").appTable({
        source: '<?php echo_uri("expenses/income_vs_expenses_summary_list_data"); ?>',
        order: [[0, "desc"]],
        dateRangeType: "yearly",
        columns: [
            {visible: false, searchable: false}, //sorting purpose only
            {title: '<?php echo lang("month") ?>', "class": "w30p", "iDataSort": 0},
            {title: '<?php echo lang("income") ?>', "class": "w20p text-right"},
            {title: '<?php echo lang("expenses") ?>', "class": "w20p text-right"},
            {title: '<?php echo lang("profit") ?>', "class": "w20p text-right"}
        ],
        printColumns: [1,2,3,4],
        xlsColumns: [1,2,3,4], 
        summation: [{column:2 , dataType: 'currency'}, {column:3 , dataType: 'currency'}, {column:4 , dataType: 'currency'}]
    });
</script>